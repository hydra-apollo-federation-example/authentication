import hydra from "hydra";
import express from "express";
import hydraExpress from 'hydra-express';
import fs from "fs";
import { gql, ApolloServer } from "apollo-server-express";
import { buildFederatedSchema } from "@apollo/federation";


async function main() {
    const config = JSON.parse((await fs.promises.readFile("./config.json")).toString());
    const version = JSON.parse((await fs.promises.readFile("./package.json")).toString()).version;

    const typeDefs = gql`
        type Query {
            me: User
        }

        type User @key(fields: "id") {
            id: ID!
            username: String
        }
    `;

    const resolvers = {
        Query: {
            me() {
                return { id: "1", username: "@ava" };
            },
        },
        User: {
            __resolveReference(user: { [key: string]: string }, {}) {
                return { id: "1", username: "@ava" };
            },
        },
    };

    const gqlServer = new ApolloServer({
        playground: false,
        introspection: true,
        schema: buildFederatedSchema([{ typeDefs, resolvers }]),
    });

    hydraExpress.init(config, version, () => {}, () => {
        const app = hydraExpress.getExpressApp();
        app.use("/test", (req, res, next) => {
            res.status(200).send("OK");
        });
        gqlServer.applyMiddleware({ app, path: "/gql" });
    });
}

main();
